const { admin } = require('./FirestoreConfig.js');

function addUser(user) {
    console.log(user);
    const db = admin.firestore();

    db.collection('users').doc(user.MobileTel)
        .set(user).then(res => console.log(res)).catch(err => console.log("Error => "+err));
}

module.exports = addUser;