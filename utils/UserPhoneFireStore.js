const { admin } = require('./FirestoreConfig.js');

async function addPhone(userPhone) {
    let status;
    console.log(userPhone);

    const db = admin.firestore();

    await db.collection('phoneout').doc(userPhone.phoneNumber)
        .set(userPhone)
        .then(res => {
            status = "success"
        })
        .catch(err => {
            status = "error"
        });
    return status;
}

module.exports = addPhone;