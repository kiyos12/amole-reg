const { admin } = require('./FirestoreConfig.js');

async function verifyCode(userPhoneWithToken) {
    let phoneNumber;
    console.log(userPhoneWithToken);

    const db = admin.firestore();
    const phoneOutRefs = db.collection("phoneout");
    await phoneOutRefs.where("verificationCode", "==", Number(userPhoneWithToken.verificationCode))
        .where("status", "==", false)
        .where("phoneNumber", "==", String(userPhoneWithToken.mobileTel) )
        .get()
        .then(function (querySnapshot) {
            querySnapshot.forEach(function (doc) {
                phoneNumber = doc.id;
                console.log(`here I am ${doc}`);
            });
        })
        .catch(function (error) {
            console.log("Error getting documents: ", error);
        });
    return phoneNumber;
}

module.exports = verifyCode;