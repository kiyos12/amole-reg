const admin = require('firebase-admin');
const serviceAccount = require("./ServiceAccounts.json");

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://amole-registration.firebaseio.com"
});

module.exports.admin = admin;