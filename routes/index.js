const addUser = require("../utils/Firestore");
const addPhone = require("../utils/UserPhoneFireStore");
const validateCode = require("../utils/ValidateCodeFireStore");
const express = require('express');
const axios = require('axios');
const router = express.Router();

/* GET home page. */
router.post('/amolereg', function (req, res, next) {
    axios({
        'url': `https://pay.meda.chat/accounts/register-proxy`,
        'headers': {
            "Content-Type": "application/json",
            "Authorization": "Basic bWVkYTp0M0BtTTNEQDIwMTghISE=",
        },
        'data': req.body,
        'method': "POST",
    }).then(response => {
        res.send(response.data);
        addUser(Object.assign(req.body, {
            Status: response.data.LongMessage,
            registeredAt: new Date().toDateString()
        }))
        console.log(response);
    }).catch(err => {
        addUser(Object.assign(req.body, {
            Status: "Server respond Error",
            registeredAt: new Date().toDateString()
        }));
        console.log(err);
        res.sendStatus(400)
    });
});

router.post('/phoneout', function (req, res) {
    const verificationCode = Math.floor(100000 + Math.random() * 900000);
    const SMSContent = {
        "phoneNumber": `${req.body.MobileTel}`,
        "text": `${verificationCode} is your verification code for your amole wallet.`
    };
    const userPhone = {
        "phoneNumber": `${req.body.MobileTel}`,
        "verificationCode": verificationCode,
        "status": false
    };
    axios({
        'url': `http://turumba.zoorya.biz:4000`,
        'headers': {
            "Content-Type": "application/json",
            "Authorization": "Basic bWVkYTp0M0BtTTNEQDIwMTghISE=",
        },
        'method': "POST",
        'data': SMSContent
    }).then(result => {
        addPhone(Object.assign(userPhone))
            .then(response => {
                console.log(response);
                res.send({
                    "message": "SMS sent"
                });
            })
            .catch(err => {
                console.log(err);
                res.send({
                    "message": "Unable to send SMS"
                });
            })
    }).catch(err => {
        console.log(err);
    });
});

router.post('/verify', function (req, res) {
    const userPhoneWithToken = {
        verificationCode: req.body.verificationCode,
        mobileTel: req.body.mobileTel
    };
    validateCode(Object.assign(userPhoneWithToken)).then((phoneNumber) => {
        console.log(phoneNumber);
        if (phoneNumber !== undefined) {
            const usedPhone = {
                "phoneNumber": `${phoneNumber}`,
                "verificationCode": req.body.verificationCode,
                "status": true
            };
            addPhone(Object.assign(usedPhone))
                .then(response => {
                    console.log(response);
                    res.send({
                        "message": "This validation code is correct"
                    });
                });
        } else {
            res.send({
                "message": "This validation code doesn't exist"
            });
        }
    });
});

module.exports = router;